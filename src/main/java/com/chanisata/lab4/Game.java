/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chanisata.lab4;

/**
 *
 * @author ASUS
 */
import java.util.Scanner;

public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        while (true) {
            printWelcome();
            boolean playAgain = true;
            while (playAgain) {
                newGame();
                boolean isFinish = false;
                while (!isFinish) {
                    printTable();
                    printTurn();
                    inputRowCol();
                    if (table.checkWin()) {
                        printTable();
                        printWinner();
                        printPlayers();
                        isFinish = true;
                    } else if (table.checkDraw()) {
                        printTable();
                        printDraw();
                        printPlayers();
                        isFinish = true;
                    } else {
                        table.switchPlayer();
                    }
                }
                playAgain = PlayAgain();
            }

            System.out.println("Goodbye");
        }
    }

    private void printWelcome() {
        System.out.println("- Welcome to OX Game -");
        System.out.println("!Start!");

    }

    private void printTable() {
        char[][] t = table.getTable();
        System.out.println("-----------------");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (j == 0) {
                    System.out.print("| " + t[i][j] + " |");
                } else if (j == 1) {
                    System.out.print("| " + t[i][j] + " |");
                } else {
                    System.out.print("| " + t[i][j] + " |");
                }
            }
            System.out.println("\n-----------------");
        }
    }

    private void printTurn() {
        System.out.println("> " + table.getCurrentPlayer().getSymbol() + " Turn <");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print(table.getCurrentPlayer().getSymbol() + "Please enter your row & col : ");
        int row = sc.nextInt();
        int col = sc.nextInt();
        table.setRowCol(row, col);
    }

    private void printWinner() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " is a Winner!");
    }

    private void printDraw() {
        System.out.println("It's a draw!");
    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);
    }

    private boolean PlayAgain() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Do you wanna play again? (Y/N): ");
        String playAgainInput = sc.nextLine().trim();
        if (playAgainInput.equalsIgnoreCase("Y")) {
            return true;
        } else if (playAgainInput.equalsIgnoreCase("N")) {
            newGame();
            return false;
        } else {
            System.out.println("Invalid input.");
            System.out.println("Please try again.");
            return PlayAgain();
        }
    }
}
